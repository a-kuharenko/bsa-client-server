'use strict';

const fs = require('fs');
const { join } = require('path');
const findUserIndexById = (id, users) => {
  let indexUser;
  users.forEach((user, index) => {
    if (+user._id === +id)
      indexUser = index;
  });
  return indexUser;
};

const saveData = data => {
  if (data) {
    const fights = JSON.parse(fs.readFileSync(
      join(__dirname, '../source', 'fightsHistory.json')
    ));
    fights.push(data);
    fs.writeFileSync(join(__dirname, '../source', 'fightsHistory.json'),
      JSON.stringify(fights), 'utf8');
    return true;
  }
  return false;
};

const updateData = (id, data) => {
  if (id && data) {
    console.log(join(__dirname, '../source', 'userlist.json'));
    const users = JSON.parse(fs.readFileSync(
      join(__dirname, '../source', 'userlist.json')
    ));
    users[findUserIndexById(id, users)] = data;
    fs.writeFileSync(join(__dirname, '../source', 'userlist.json'),
      JSON.stringify(users), 'utf8');
    return true;
  }
  return false;
};

const deleteData = id => {
  if (id) {
    const users = JSON.parse(fs.readFileSync(
      join(__dirname, '../source', 'userlist.json')
    ));
    users.splice(findUserIndexById(id, users), 1);
    fs.writeFileSync(join(__dirname, '../source', 'userlist.json'),
      JSON.stringify(users), 'utf8');
    return true;
  }
  return false;
};

module.exports = { saveData, updateData, deleteData, findUserIndexById };
