'use strict';

const { saveData, updateData,
  deleteData, findUserIndexById } = require('../repositories/user.repository');
const fs = require('fs');
const { join } = require('path');

const getUsers = id => {
  const users = JSON.parse(fs.readFileSync(
    join(__dirname, '../source', 'userlist.json')
  ));
  if (!id)
    return users;
  else
    return users[findUserIndexById(id, users)];
};

const addFight = fight => {
  if (fight)
    return saveData(fight);
  return null;
};

const updateUser = (id, user) => {
  if (id && user)
    return updateData(id, user);
  return null;
};

const deleteFighter = id => {
  if (id)
    return deleteData(id);
  return null;
};
module.exports = { getUsers, addFight, updateUser, deleteFighter };
