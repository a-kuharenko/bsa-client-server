'use strict';

const fs = require('fs');
const { join } = require('path');
const hasId = (req, res, next) => {
  const id = req.params.id;
  let hasId = false;
  const users = JSON.parse(fs.readFileSync(
    join(__dirname, '../source', 'userlist.json')
  ));
  users.forEach(user => {
    if (user._id === id)
      hasId = true;
  });
  if (hasId)
    next();
  else
    res.status(400).send(`No user with this id((${id})`);
};

module.exports = { hasId };
