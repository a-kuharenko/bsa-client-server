'use strict';

const express = require('express');
const router = express.Router();
const { join } = require('path');
/* GET home page. */
router.get('/', (req, res, next) => {
  res.sendFile(join(__dirname, '../../', 'index.html'));
});

module.exports = router;
