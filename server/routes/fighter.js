'use strict';

const express = require('express');
const router = express.Router();
const { getUsers, addFight,
  updateUser, deleteFighter } = require('../services/user.service');
const { hasId } = require('../middlewares/id.middlewares');
/* GET users listing. */


router.get('/', (req, res, next) => {
  const result = getUsers();
  if (result)
    res.send(result);
  else
    res.status(400).send({ msg: 'Error in getting users' });
});

router.get('/:id', (req, res, next) => {
  const result = getUsers(req.params.id);

  if (result)
    res.send(result);
  else
    res.status(400).send(`No user with such id(${req.params.id})`);
});

router.post('/', (req, res, next) => {
  const result = addFight(req.body);

  if (result)
    res.send({ msg: `Fight ${JSON.stringify(req.body)} was added to history` });
  else
    res.status(400).send({ msg: 'Error in creating user' });
});

router.put('/:id', hasId, (req, res, next) => {
  const result = updateUser(req.params.id, req.body);
  if (result)
    res.send({ msg: `User with ${req.params.id} id was updated` });
  else
    res.status(400).send({ msg: 'Error in updating user' });
});

router.delete('/:id', hasId, (req, res, next) => {
  const result = deleteFighter(req.params.id);

  if (result)
    res.send({ msg: `User with ${req.params.id} id was deleted` });
  else
    res.status(400).send({ msg: 'Error in deleting user' });
});

module.exports = router;
