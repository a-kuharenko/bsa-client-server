# bsa-client-server
**Attempt to connect server with the previous task.**

## Instalation

1. `git clone https://a-kuharenko@bitbucket.org/a-kuharenko/bsa-client-server.git`
2. `cd bsa-client-server`
3. `npm i`
4. `npm run build; npm run server` or `npm start`
5. 	By default server running on [localhost:3000](http://localhost:3000)


### Server can handle the list of following requests:

* ##**GET**: `/fighter`  
(get an array of all fighters)

* ##**GET:** `/fighter/:id`  
(get an fighter from array by id)

* ##**POST:** `/`  
(add fight to the history from the reques body)

* ##**PUT:** `/fighter/:id`  
(update fighter info according to the data from the request body)

* ##**DELETE:** `/fighter/:id`  
(delete one fighter by ID)