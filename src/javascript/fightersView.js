import View from './view';
import FighterView from './fighterView';
import { fighterService } from './services/fightersService';
import showDetails from './showDetails'
class FightersView extends View {
  constructor(fighters) {
    super();
    
    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);

    this.fight = [];
    this.fighters = fighters;
  }

  fightersDetailsMap = new Map();

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });
    this.fighterElements = fighterElements;
    this.element = this.createElement({ tagName: 'div', className: 'fighters'});
    this.element.append(...fighterElements);
  }
  _chooseFighter(fighter){
    const defaultHEIGHT = '260px';
    const selectedHEIGHT = '320px';
    const borderSTYLE = '3px solid yellow';

    const imageElements = document.getElementsByClassName('fighter-image');
    findUserIndexById = findUserIndexById.bind(this);
    const index = findUserIndexById(fighter._id, this.fighters);

    function select(index, fighter){
      this.fight.push(fighter);
      this.fighterElements[index].style.border = borderSTYLE;
      imageElements[index].style.height = selectedHEIGHT;
    }
    select = select.bind(this);

    function unselect(index, fighter){
      if(fighter)
        this.fight.splice(this.fight.indexOf(fighter), 1);
      this.fighterElements[index].style.border = '';
      imageElements[index].style.height = defaultHEIGHT;
    }
    unselect = unselect.bind(this);

    if(!fighter.selected){
      if(this.fight.length === 2){ 
        const f1 = this.fight.shift();
        const index = findUserIndexById(f1._id, this.fighters);
        unselect(index);
        f1.selected = !f1.selected;
      }
      select(index, fighter);
    }
    else
      unselect(index, fighter);
    fighter.selected = !fighter.selected;
  }
  async handleFighterClick(event, fighter) {
    if(!fighter.selected){
      let details = await fighterService.getFighterDetails(fighter._id);
      showDetails(details, function(updatedDetails) {
        if(updatedDetails)
          details = updatedDetails;
      })
      this.fightersDetailsMap.set(fighter._id, details);
    }
    this._chooseFighter(fighter);
    this.fight.length === 2 ?
      document.getElementById('start-button').disabled = false
      : document.getElementById('start-button').disabled = true;
  }
}

function findUserIndexById(id,users){
  let indexUser;
  users.forEach((user,index) => {
      if(+user._id === +id)
          indexUser = index;
  });
  return indexUser;
}

export default FightersView;