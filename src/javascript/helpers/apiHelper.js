const API_URL = 'http://localhost:3000/';

function callApi(endpoind, method, body) {
  const url = API_URL + endpoind;
  let options;
  if(body){
    options = {
      method,
      body,
      headers: { "Content-Type": "application/json" }
    };
  }
  else{
    options = {
      method,
      headers: { "Content-Type": "application/json" }
    };
  }

  return fetch(url, options)
    .then(response =>{
      console.log(response.ok)
      return response.ok ? response.json() : Promise.reject(Error('Failed to load'))
    })
    .catch(error => {
      throw error;
    });
}

export { callApi }