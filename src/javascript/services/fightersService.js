import { callApi } from '../helpers/apiHelper';

class FighterService {
  async getFighters() {
    try {
      const endpoint = 'fighter';
      const apiResult = await callApi(endpoint, 'GET');
      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(_id) {
    try{
      const endpoint = `fighter/${_id}`;
      const apiResult = await callApi(endpoint, 'GET');

      return apiResult;
    } catch(error) {
      throw error;
    }
  }

  async updateFighterDetails(_id, body) {
    try{
      const endpoint = `fighter/${_id}`;
      const apiResult = await callApi(endpoint, 'PUT', body);

      return apiResult;
    } catch(error) {
      throw error;
    }
  }

  async addFight(body) {
    try{
      const endpoint = `fighter`;
      const apiResult = await callApi(endpoint, 'POST', body);

      return apiResult;
    } catch(error) {
      throw error;
    }
  }

  async deleteFighter(id) {
    try{
      const endpoint = `fighter/${id}`;
      const apiResult = await callApi(endpoint, 'DELETE');

      return apiResult;
    } catch(error) {
      throw error;
    }
  }
}

export const fighterService = new FighterService();
