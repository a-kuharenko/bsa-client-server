class Fighter {
  constructor(details) {
    this.name = details.name;
    this.health = details.health;
    this.attack = details.attack;
    this.defense = details.defense;
    this.source = details.source;
  }
  getHitPower() {
    const criticalHitChance = getRandom(1, 3);
    return this.attack * criticalHitChance;
  }
  getBlockPower() {
    const dodgeChance = getRandom(1, 3);
    return this.defense * dodgeChance;
  }
}

function getRandom(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

export default Fighter;
